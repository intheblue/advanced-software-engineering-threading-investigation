#pragma once
#include <map>
#include <memory>
#include <list>
#include <algorithm>
#include <iostream>
#include <sstream>

#include "DataSet.h"

std::list<std::shared_ptr<Record>> DataSet::getSample() const
{
    return chosen;
}

unsigned long DataSet::calc(std::shared_ptr<Record> rec)
{
	unsigned long wt = 5;
	wt += (learners.find(rec->id) == learners.end() ? 0 : 1);
	wt += (assessors.find(rec->ass) == assessors.end() ? 0 : 2);
	wt += (modules.find(rec->mod) == modules.end() ? 0 : 2);
	return wt;
}

void DataSet::applyWeightsAndMoveWinner()
{
	if(pool.size() > 0)
	{
		unsigned long maxWtFound = 0;
		unsigned long whichRecord;
	
		for (std::shared_ptr<Record> r : pool)
		{
			r->weight = calc(r);
			if (r->weight > maxWtFound)
			{
				maxWtFound = r->weight;
				whichRecord = r->rowNo;
			}
		}
		auto winningRec = std::find_if(pool.begin(), pool.end(), [=](std::shared_ptr<Record> r) {return (maxWtFound==r->weight);});
		std::shared_ptr<Record> winner = *winningRec;
		learners.erase(winner->id);
		assessors.erase(winner->ass);
		modules.erase(winner->mod);
		chosen.splice(chosen.begin(), pool, winningRec);
	}
}

void DataSet::extractSample()
{
    numSamplesNeeded = std::max(6, int(ceil(learners.size()*0.1)));
    while ((!modules.empty() || !assessors.empty() || chosen.size() < numSamplesNeeded) && !pool.empty())
	{
		std::cout << '.';
		applyWeightsAndMoveWinner();
    }
    std::cout << std::endl;
}

void DataSet::add(std::vector<std::string> rec)
{
	std::istringstream iss(rec[0]);
	//As we're using shared_ptrs, we need to allocate the new record object on the heap and wrap it
	std::shared_ptr<Record> r = std::shared_ptr<Record>(new Record);
	iss >> r->rowNo;
	learners.insert(rec[2]);
	r->id = rec[2];
	assessors.insert(rec[3]);
	r->ass = rec[3];
	modules.insert(rec[4]);
	r->mod = rec[4];
	pool.push_back(r);
}