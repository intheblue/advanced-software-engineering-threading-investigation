#pragma once
#include <vector>
#include <thread>
#include <condition_variable>
#include "ConcurrentQueue.h"

class ThreadPool
{
public:
	ThreadPool(size_t numThreads);
	
	template<class F>
	void enqueue(F func);
	bool tasksEmpty();
	~ThreadPool();
private:
	std::vector<std::thread> workers;
	ConcurrentQueue<std::function<void()>> tasks;
	std::condition_variable condition;
	bool stop;
};

ThreadPool::ThreadPool(size_t numThreads) : stop(false)
{
	for(size_t i = 0; i < numThreads; i++)
	{
		workers.push_back(std::thread(
		[this]
		{
			while(true)
			{
				//We only want workers to stop when they've been signalled to and there are no more remaining tasks
				if(this->stop && this->tasks.Empty())
				{
					return;
				}
				std::function<void()> task(this->tasks.Pop());
				task();
			}
		}));
	}
}

ThreadPool::~ThreadPool()
{
	stop = true;
	condition.notify_all(); //Wake up any waiting threads after stop has been set

	// join all the now woken up threads
	for(size_t i = 0; i < workers.size(); i++)
	{
		workers[i].join();
	}
}

template<class Func>
void ThreadPool::enqueue(Func f)
{
	tasks.Push(std::function<void()>(f));
	condition.notify_one();
}

bool ThreadPool::tasksEmpty()
{
	return tasks.Empty();
}