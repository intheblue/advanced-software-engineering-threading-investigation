#pragma once
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <chrono>
#include <thread>
#include <memory>
#include "DataSet.h"
#include "ThreadPool.h"

typedef std::map<std::string, std::shared_ptr<DataSet>> ABCDatabase;

void readRobust(ABCDatabase & someTable, std::string fn)
{
	std::string line, word;
	std::ifstream inf;
	std::vector<std::string> currRec;
	inf.open(fn.c_str());
	while (! inf.eof() )
	{
		currRec.clear();
		getline(inf, line);
		std::istringstream iss(line);
		while (iss >> word)
		{
			currRec.push_back(word);
		}
		if (currRec.size() == 5)
		{
            std::string key = currRec[1];
			//Lazy initialisation with a map and shared_ptrs doesn't work, so we need to create the dataset shared_ptr if it doesn't exist
			if(someTable.count(key) == 0)
			{
				someTable[key] = std::shared_ptr<DataSet>(new DataSet());
			}
			std::shared_ptr<DataSet> someDataSet = someTable[key];
			someDataSet->add(currRec);
		}
	}
}

void saveResults(const ABCDatabase & aTable, std::string fn)
{
    std::ofstream out;
    out.open(fn.c_str());
    for (auto dsPair : aTable)
    {
		out << dsPair.first << "   Allocation Group" << std::endl;
        for (auto rec : dsPair.second->getSample())
            out << rec->id <<" :learner\t" << rec->ass  << " :assessor\t" <<rec->mod <<" :module"<< std::endl;
        out << std::endl;
    }
    out.close();
}

//Utility function: C++11 supports the round function, but not implemented by VS
int round(float f)
{
	return static_cast<int>(f + 0.5);
}

int main()
{
	ABCDatabase table;
    std::string filename("Book1.txt");
    std::string outFile("samples.txt");
    auto phaseOneStart = std::chrono::steady_clock::now();
	readRobust(table, filename);
    auto phaseOneEnd = std::chrono::steady_clock::now();

	//std::thread::hardware_concurrency will return the number of concurrent threads supported by the CPU, 
	// including any supported by Intel Hyper-Threading.
	size_t coreCount = std::thread::hardware_concurrency();
	ThreadPool* tp = new ThreadPool(coreCount);
	for(auto tableIt = table.begin(); tableIt != table.end(); tableIt++)
	{
		std::shared_ptr<DataSet> ds = tableIt->second;
		//Wrap the extractSample method of a DataSet object in a lambda that copy-captures the shared_ptr for the DataSet.
		//This is much easier and more readable than creating a pointer-to-member function and passing that around
		//Also prevents issues caused by trying to call the method from the thread function
		tp->enqueue([=]{ds->extractSample();});
	}
	//Loop main thread until the ThreadPool tasks queue is empty, then call the ThreadPool destructor
	while(true)
	{
		if(tp->tasksEmpty())
		{
			delete tp;
			break;
		}
	}

    saveResults(table, outFile);
    auto phaseTwoEnd = std::chrono::steady_clock::now();
    auto phaseOneElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(phaseOneEnd - phaseOneStart);
    auto phaseTwoElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(phaseTwoEnd - phaseOneEnd);
    std::cout << "Phase one : " << float(phaseOneElapsed.count()/1000.0) << std::endl;
    std::cout << "Phase two : " << phaseTwoElapsed.count()/1000.0 << std::endl;
}
