#pragma once
#include <string>
#include <vector>
#include <list>
#include <unordered_set>
#include <memory>

struct Record
{
	unsigned rowNo;
	std::string id;
	std::string ass;
	std::string mod;
	unsigned long  weight;
};

class DataSet
{
public:
	void add(std::vector<std::string>);
	void applyWeightsAndMoveWinner();
    void extractSample();
    std::list<std::shared_ptr<Record>> getSample() const;
private:
	std::unordered_set<std::string> learners;
	std::unordered_set<std::string> assessors;
	std::unordered_set<std::string> modules;
	std::list<std::shared_ptr<Record>> pool, chosen;
	unsigned long calc(std::shared_ptr<Record> rec);
    unsigned numSamplesNeeded;
};