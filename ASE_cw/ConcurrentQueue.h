#pragma once
#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>

template <class T>
class ConcurrentQueue
{
	/*
	This replaces a non-concurrent queue owned by the ThreadPool with locking inside the ThreadPool methods
	By keeping all locking inside this class, there are likely to be fewer issues caused by forgetting to lock the queue during access
	Generic container that will cope with primitives, function objects or shared_ptrs. 
	As the internal container is an STL container, raw pointers should not be used, always use shared_ptrs. It's what they're for.
	*/
	std::queue<T> mQueue;
	mutable std::mutex mMutex;
	std::condition_variable mEmptyCondition;
public:
	void Push(T value);
	T Pop(); //Will block until an item is available in the queue
	T TryPop(); //Will not block, returns result of default constructor call to the template type. Eg: Null shared_ptr is storing shared_ptrs
	bool Empty() const;
};

template <class T>
void ConcurrentQueue<T>::Push(T value)
{
	//lock_guards automatically release the lock on the mutex when they go out of scope. Completely exception safe
	std::lock_guard<std::mutex> lock(mMutex);
	mQueue.push(value);
	//Wake up a thread blocked on Pop if any are sleeping to consume this item
	mEmptyCondition.notify_one();
}

template <class T>
T ConcurrentQueue<T>::Pop()
{
	std::unique_lock<std::mutex> lock(mMutex);
	//Wait needs a predicate that returns false if we need to keep waiting. Being woken up doesn't guarantee there is still work available, so we check there is something in the queue 
	mEmptyCondition.wait(lock, [this]{return !mQueue.empty();});
	T ret = mQueue.front();
	mQueue.pop();
	return ret;
}

template <class T>
T ConcurrentQueue<T>::TryPop()
{
	std::lock_guard<std::mutex> lock(mMut);
	if(mQueue.empty())
	{
		return T();
	}
	T ret = mQueue.front();
	mQueue.pop();
	return ret;
}

template <class T>
bool ConcurrentQueue<T>::Empty() const
{
	//This modifies the state of mMutex, but it is marked mutable to allow this
	std::lock_guard<std::mutex> lock(mMutex); 
	return mQueue.empty();
}